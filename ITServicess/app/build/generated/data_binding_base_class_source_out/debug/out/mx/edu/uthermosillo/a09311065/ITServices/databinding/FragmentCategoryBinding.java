// Generated by view binder compiler. Do not edit!
package mx.edu.uthermosillo.a09311065.ITServices.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.facebook.shimmer.ShimmerFrameLayout;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;
import mx.edu.uthermosillo.a09311065.ITServices.R;

public final class FragmentCategoryBinding implements ViewBinding {
  @NonNull
  private final ConstraintLayout rootView;

  @NonNull
  public final ShimmerFrameLayout categoriesShimmer;

  @NonNull
  public final TextView categoryName;

  @NonNull
  public final TextView categoryStatus;

  @NonNull
  public final ConstraintLayout content;

  @NonNull
  public final Guideline vertGuide;

  private FragmentCategoryBinding(@NonNull ConstraintLayout rootView,
      @NonNull ShimmerFrameLayout categoriesShimmer, @NonNull TextView categoryName,
      @NonNull TextView categoryStatus, @NonNull ConstraintLayout content,
      @NonNull Guideline vertGuide) {
    this.rootView = rootView;
    this.categoriesShimmer = categoriesShimmer;
    this.categoryName = categoryName;
    this.categoryStatus = categoryStatus;
    this.content = content;
    this.vertGuide = vertGuide;
  }

  @Override
  @NonNull
  public ConstraintLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static FragmentCategoryBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static FragmentCategoryBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.fragment_category, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static FragmentCategoryBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.categories_shimmer;
      ShimmerFrameLayout categoriesShimmer = ViewBindings.findChildViewById(rootView, id);
      if (categoriesShimmer == null) {
        break missingId;
      }

      id = R.id.category_name;
      TextView categoryName = ViewBindings.findChildViewById(rootView, id);
      if (categoryName == null) {
        break missingId;
      }

      id = R.id.category_status;
      TextView categoryStatus = ViewBindings.findChildViewById(rootView, id);
      if (categoryStatus == null) {
        break missingId;
      }

      id = R.id.content;
      ConstraintLayout content = ViewBindings.findChildViewById(rootView, id);
      if (content == null) {
        break missingId;
      }

      id = R.id.vert_guide;
      Guideline vertGuide = ViewBindings.findChildViewById(rootView, id);
      if (vertGuide == null) {
        break missingId;
      }

      return new FragmentCategoryBinding((ConstraintLayout) rootView, categoriesShimmer,
          categoryName, categoryStatus, content, vertGuide);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
