package mx.edu.uthermosillo.a09311065.ITServices.ui.views

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import mx.edu.uthermosillo.a09311065.ITServices.R

public class DashboardFragmentDirections private constructor() {
  public companion object {
    public fun actionDashboardFragmentToProductsFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_dashboardFragment_to_productsFragment)

    public fun actionDashboardFragmentToCategoriesFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_dashboardFragment_to_categoriesFragment)
  }
}
