package mx.edu.uthermosillo.a09311065.ITServices.ui.views.categories

import android.os.Bundle
import androidx.navigation.NavDirections
import kotlin.Int
import mx.edu.uthermosillo.a09311065.ITServices.R

public class CategoriesFragmentDirections private constructor() {
  private data class ActionCategoriesFragmentToCategoryFragment(
    public val categoryId: Int = 0,
  ) : NavDirections {
    public override val actionId: Int = R.id.action_categoriesFragment_to_categoryFragment

    public override val arguments: Bundle
      get() {
        val result = Bundle()
        result.putInt("categoryId", this.categoryId)
        return result
      }
  }

  public companion object {
    public fun actionCategoriesFragmentToCategoryFragment(categoryId: Int = 0): NavDirections =
        ActionCategoriesFragmentToCategoryFragment(categoryId)
  }
}
