package mx.edu.uthermosillo.a09311065.ITServices.ui.views.categories

import android.os.Bundle
import androidx.lifecycle.SavedStateHandle
import androidx.navigation.NavArgs
import java.lang.IllegalArgumentException
import kotlin.Int
import kotlin.jvm.JvmStatic

public data class CategoryFragmentArgs(
  public val categoryId: Int = 0,
) : NavArgs {
  public fun toBundle(): Bundle {
    val result = Bundle()
    result.putInt("categoryId", this.categoryId)
    return result
  }

  public fun toSavedStateHandle(): SavedStateHandle {
    val result = SavedStateHandle()
    result.set("categoryId", this.categoryId)
    return result
  }

  public companion object {
    @JvmStatic
    public fun fromBundle(bundle: Bundle): CategoryFragmentArgs {
      bundle.setClassLoader(CategoryFragmentArgs::class.java.classLoader)
      val __categoryId : Int
      if (bundle.containsKey("categoryId")) {
        __categoryId = bundle.getInt("categoryId")
      } else {
        __categoryId = 0
      }
      return CategoryFragmentArgs(__categoryId)
    }

    @JvmStatic
    public fun fromSavedStateHandle(savedStateHandle: SavedStateHandle): CategoryFragmentArgs {
      val __categoryId : Int?
      if (savedStateHandle.contains("categoryId")) {
        __categoryId = savedStateHandle["categoryId"]
        if (__categoryId == null) {
          throw IllegalArgumentException("Argument \"categoryId\" of type integer does not support null values")
        }
      } else {
        __categoryId = 0
      }
      return CategoryFragmentArgs(__categoryId)
    }
  }
}
