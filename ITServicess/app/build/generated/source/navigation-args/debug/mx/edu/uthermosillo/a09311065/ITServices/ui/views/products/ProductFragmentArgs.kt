package mx.edu.uthermosillo.a09311065.ITServices.ui.views.products

import android.os.Bundle
import androidx.lifecycle.SavedStateHandle
import androidx.navigation.NavArgs
import java.lang.IllegalArgumentException
import kotlin.Int
import kotlin.jvm.JvmStatic

public data class ProductFragmentArgs(
  public val productId: Int = 0,
) : NavArgs {
  public fun toBundle(): Bundle {
    val result = Bundle()
    result.putInt("productId", this.productId)
    return result
  }

  public fun toSavedStateHandle(): SavedStateHandle {
    val result = SavedStateHandle()
    result.set("productId", this.productId)
    return result
  }

  public companion object {
    @JvmStatic
    public fun fromBundle(bundle: Bundle): ProductFragmentArgs {
      bundle.setClassLoader(ProductFragmentArgs::class.java.classLoader)
      val __productId : Int
      if (bundle.containsKey("productId")) {
        __productId = bundle.getInt("productId")
      } else {
        __productId = 0
      }
      return ProductFragmentArgs(__productId)
    }

    @JvmStatic
    public fun fromSavedStateHandle(savedStateHandle: SavedStateHandle): ProductFragmentArgs {
      val __productId : Int?
      if (savedStateHandle.contains("productId")) {
        __productId = savedStateHandle["productId"]
        if (__productId == null) {
          throw IllegalArgumentException("Argument \"productId\" of type integer does not support null values")
        }
      } else {
        __productId = 0
      }
      return ProductFragmentArgs(__productId)
    }
  }
}
