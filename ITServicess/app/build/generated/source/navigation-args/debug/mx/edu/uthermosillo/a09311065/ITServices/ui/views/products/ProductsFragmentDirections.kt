package mx.edu.uthermosillo.a09311065.ITServices.ui.views.products

import android.os.Bundle
import androidx.navigation.NavDirections
import kotlin.Int
import mx.edu.uthermosillo.a09311065.ITServices.R

public class ProductsFragmentDirections private constructor() {
  private data class ActionProductsFragmentToProductFragment(
    public val productId: Int = 0,
  ) : NavDirections {
    public override val actionId: Int = R.id.action_productsFragment_to_productFragment

    public override val arguments: Bundle
      get() {
        val result = Bundle()
        result.putInt("productId", this.productId)
        return result
      }
  }

  public companion object {
    public fun actionProductsFragmentToProductFragment(productId: Int = 0): NavDirections =
        ActionProductsFragmentToProductFragment(productId)
  }
}
