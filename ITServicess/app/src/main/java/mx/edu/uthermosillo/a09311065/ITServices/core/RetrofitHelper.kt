package mx.edu.uthermosillo.a09311065.ITServices.core

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitHelper {
    fun getRetrofit() : Retrofit{

        return Retrofit.Builder()
            .baseUrl("https://gonzalez.terrabyteco.com/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}