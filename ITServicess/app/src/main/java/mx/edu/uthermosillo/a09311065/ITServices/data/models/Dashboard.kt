package mx.edu.uthermosillo.a09311065.ITServices.data.models

data class Dashboard(
    var id: Int,
    var name: String,
    var image: String
)
