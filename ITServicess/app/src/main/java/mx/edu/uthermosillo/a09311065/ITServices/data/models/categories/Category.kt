package mx.edu.uthermosillo.a09311065.ITServices.data.models.categories

import com.google.gson.annotations.SerializedName

data class Category(
    @SerializedName("id") val id: Int,
    @SerializedName ("name") val name: String,
    @SerializedName ("status") val status: String
)
