package mx.edu.uthermosillo.a09311065.ITServices.data.models.products

import com.google.gson.annotations.SerializedName
import mx.edu.uthermosillo.a09311065.ITServices.data.models.categories.Category

data class Product(
    @SerializedName ("id") val id: Int,
    @SerializedName ("name") val name: String,
    @SerializedName ("description") val description: String,
    @SerializedName ("price") val price: String,
    @SerializedName ("available") val available: String,
    @SerializedName ("status") val status: Int,
    @SerializedName ("image") val image: String,
    @SerializedName ("category") val category: Category

)


