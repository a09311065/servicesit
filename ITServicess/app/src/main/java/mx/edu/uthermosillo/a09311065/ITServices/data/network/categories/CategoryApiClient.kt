package mx.edu.uthermosillo.a09311065.ITServices.data.network.categories

import mx.edu.uthermosillo.a09311065.ITServices.data.models.categories.Category
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface CategoryApiClient {
    @GET("categories")
    suspend fun getAllCategories() : Response<List<Category>>

    @GET("categories/{id}")
    suspend fun getCategory(@Path("id") id: Int) : Response<Category>
}