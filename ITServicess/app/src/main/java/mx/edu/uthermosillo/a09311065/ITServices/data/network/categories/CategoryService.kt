package mx.edu.uthermosillo.a09311065.ITServices.data.network.categories

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import mx.edu.uthermosillo.a09311065.ITServices.core.RetrofitHelper
import mx.edu.uthermosillo.a09311065.ITServices.data.models.categories.Category

class CategoryService {
    private val retrofit = RetrofitHelper.getRetrofit()
    suspend fun getCategories() : List<Category>{
        return withContext(Dispatchers.IO){
            val response = retrofit.create(CategoryApiClient::class.java).getAllCategories()
            response.body() ?: emptyList()
        }
    }

    suspend fun getCategory(category_id: Int) : Category {
        return withContext(Dispatchers.IO){
            val response = retrofit.create(CategoryApiClient::class.java).getCategory(category_id)
            response.body()!!
        }
    }
}