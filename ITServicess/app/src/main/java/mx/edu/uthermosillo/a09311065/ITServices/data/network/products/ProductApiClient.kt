package mx.edu.uthermosillo.a09311065.ITServices.data.network.products

import mx.edu.uthermosillo.a09311065.ITServices.data.models.products.Product
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface ProductApiClient {
    @GET("products")
    suspend fun getAllProducts() : Response<List<Product>>

    @GET("products/{id}")
    suspend fun getProduct(@Path("id") id: Int) : Response<Product>


}