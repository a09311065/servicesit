package mx.edu.uthermosillo.a09311065.ITServices.data.network.products

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import mx.edu.uthermosillo.a09311065.ITServices.core.RetrofitHelper
import mx.edu.uthermosillo.a09311065.ITServices.data.models.products.Product

class ProductService {
    private val retrofit = RetrofitHelper.getRetrofit()
    suspend fun getProducts() : List<Product>{
        return withContext(Dispatchers.IO){
            val response = retrofit.create(ProductApiClient::class.java).getAllProducts()
            response.body() ?: emptyList()
        }
    }

    suspend fun getProduct(product_id: Int) : Product {
        return withContext(Dispatchers.IO){
            val response = retrofit.create(ProductApiClient::class.java).getProduct(product_id)
            response.body()!!
        }
    }
}