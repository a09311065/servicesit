package mx.edu.uthermosillo.a09311065.ITServices.data.repositories

import mx.edu.uthermosillo.a09311065.ITServices.data.models.categories.CategoriesProvider
import mx.edu.uthermosillo.a09311065.ITServices.data.models.categories.CategoryProvider
import mx.edu.uthermosillo.a09311065.ITServices.data.models.categories.Category
import mx.edu.uthermosillo.a09311065.ITServices.data.network.categories.CategoryService


class CategoryRepository {
    private val api = CategoryService()
    suspend fun getAllCategories(): List<Category>{
        val response = api.getCategories()
        CategoriesProvider.categories = response
        return response
    }

    suspend fun getCategory(category_id: Int): Category {
        val response = api.getCategory(category_id)
        CategoryProvider.category = response
        return response
    }
}