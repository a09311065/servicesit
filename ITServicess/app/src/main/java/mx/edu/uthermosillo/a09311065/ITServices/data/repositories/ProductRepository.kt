package mx.edu.uthermosillo.a09311065.ITServices.data.repositories

import mx.edu.uthermosillo.a09311065.ITServices.data.models.products.Product
import mx.edu.uthermosillo.a09311065.ITServices.data.models.products.ProductProvider
import mx.edu.uthermosillo.a09311065.ITServices.data.models.products.ProductsProvider
import mx.edu.uthermosillo.a09311065.ITServices.data.network.products.ProductService


class ProductRepository {
    private val api = ProductService()
    suspend fun getAllProducts(): List<Product>{
        val response = api.getProducts()
        ProductsProvider.products = response
        return response
    }

    suspend fun getProduct(product_id: Int): Product {
        val response = api.getProduct(product_id)
        ProductProvider.product = response
        return response
    }
}