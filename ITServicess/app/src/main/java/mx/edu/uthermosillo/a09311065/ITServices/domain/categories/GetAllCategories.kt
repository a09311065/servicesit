package mx.edu.uthermosillo.a09311065.ITServices.domain.categories

import mx.edu.uthermosillo.a09311065.ITServices.data.repositories.CategoryRepository

class GetAllCategories {
    private val repository = CategoryRepository()

    suspend operator fun invoke() = repository.getAllCategories()
}