package mx.edu.uthermosillo.a09311065.ITServices.domain.categories

import mx.edu.uthermosillo.a09311065.ITServices.data.repositories.CategoryRepository


class GetCategory {
    private val repository = CategoryRepository()

    suspend operator fun invoke(category_id: Int) = repository.getCategory(category_id)
}