package mx.edu.uthermosillo.a09311065.ITServices.domain.products

import mx.edu.uthermosillo.a09311065.ITServices.data.repositories.ProductRepository

class GetAllProducts {
    private val repository = ProductRepository()

    suspend operator fun invoke() = repository.getAllProducts()
}