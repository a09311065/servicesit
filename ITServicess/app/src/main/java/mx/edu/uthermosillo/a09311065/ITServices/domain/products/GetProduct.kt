package mx.edu.uthermosillo.a09311065.ITServices.domain.products

import mx.edu.uthermosillo.a09311065.ITServices.data.repositories.ProductRepository

class GetProduct {
    private val repository = ProductRepository()

    suspend operator fun invoke(product_id: Int) = repository.getProduct(product_id)
}