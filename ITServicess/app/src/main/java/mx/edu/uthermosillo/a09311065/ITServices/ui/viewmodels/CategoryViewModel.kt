package mx.edu.uthermosillo.a09311065.ITServices.ui.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import mx.edu.uthermosillo.a09311065.ITServices.domain.categories.GetAllCategories
import mx.edu.uthermosillo.a09311065.ITServices.data.models.categories.Category
import mx.edu.uthermosillo.a09311065.ITServices.domain.categories.GetCategory

class CategoryViewModel: ViewModel() {
    val categories = MutableLiveData<List<Category>>()
    val isLoading = MutableLiveData<Boolean>()
    var category = MutableLiveData<Category>()

    var getAllCategories = GetAllCategories()
    var getCategory = GetCategory()

    fun loadCategory(category_id: Int) {
        viewModelScope.launch {
            var result = getCategory.invoke(category_id)
            isLoading.postValue(true)
            category.postValue(result)
            isLoading.postValue(false)
        }
    }

    fun loadCategories() {
        viewModelScope.launch {
            isLoading.postValue(true)
            val result = getAllCategories()

            if(!result.isNullOrEmpty()){
                categories.postValue(result)
                isLoading.postValue(false)
            }
        }
    }
}