package mx.edu.uthermosillo.a09311065.ITServices.ui.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import mx.edu.uthermosillo.a09311065.ITServices.data.models.products.Product
import mx.edu.uthermosillo.a09311065.ITServices.domain.products.GetAllProducts
import mx.edu.uthermosillo.a09311065.ITServices.domain.products.GetProduct

class ProductViewModel : ViewModel() {
    val products = MutableLiveData<List<Product>>()
    val isLoading = MutableLiveData<Boolean>()
    var product = MutableLiveData<Product>()

    var getAllProducts = GetAllProducts()
    var getProduct = GetProduct()

    fun loadProduct(product_id: Int) {
        viewModelScope.launch {
            var result = getProduct.invoke(product_id)
            isLoading.postValue(true)
            product.postValue(result)
            isLoading.postValue(false)
        }
    }

    fun loadProducts() {
        viewModelScope.launch {
            isLoading.postValue(true)
            val result = getAllProducts()

            if(!result.isNullOrEmpty()){
                products.postValue(result)
                isLoading.postValue(false)
            }
        }
    }
}