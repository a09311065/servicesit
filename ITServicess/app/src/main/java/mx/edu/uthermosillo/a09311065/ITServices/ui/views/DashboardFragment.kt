package mx.edu.uthermosillo.a09311065.ITServices.ui.views

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import mx.edu.uthermosillo.a09311065.ITServices.ui.views.adapters.DashboardAdapter
import mx.edu.uthermosillo.a09311065.ITServices.databinding.FragmentDashboardBinding
import mx.edu.uthermosillo.a09311065.ITServices.data.models.Dashboard

class DashboardFragment : Fragment() {

    private  var _binding: FragmentDashboardBinding? = null
    private  val binding get() = _binding!!


    private lateinit var  dashboardAdapter: DashboardAdapter
    private  var options: List<Dashboard> = emptyList()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        _binding = FragmentDashboardBinding.inflate(inflater, container, false)
        loadData()
        val recyclerView = binding.dashboardRecycler
        dashboardAdapter = DashboardAdapter(options)
        //Para mostrarlo en lista
        val layoutManager = LinearLayoutManager(context)
        //Para mostrarlo en cuadricula
        //val layoutManager = GridLayoutManager(context, 2)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = dashboardAdapter


        return binding.root
    }

    private fun loadData(){
        options = listOf(
            Dashboard(1,"Products", "https://e7.pngegg.com/pngimages/184/768/png-clipart-enterprise-service-management-managed-services-it-service-management-business-company-service.png"),
            Dashboard(2,"Categories", "https://w7.pngwing.com/pngs/611/260/png-transparent-arrow-category-arrow-classification-title.png")
        )
    }

}