package mx.edu.uthermosillo.a09311065.ITServices.ui.views.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import mx.edu.uthermosillo.a09311065.ITServices.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}