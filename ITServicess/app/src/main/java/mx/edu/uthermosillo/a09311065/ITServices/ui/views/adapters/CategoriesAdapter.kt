package mx.edu.uthermosillo.a09311065.ITServices.ui.views.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import mx.edu.uthermosillo.a09311065.ITServices.R
import mx.edu.uthermosillo.a09311065.ITServices.databinding.ItemCategoryBinding
import mx.edu.uthermosillo.a09311065.ITServices.data.models.categories.Category
import mx.edu.uthermosillo.a09311065.ITServices.ui.views.categories.CategoriesFragmentDirections

class CategoriesAdapter (private val categories : List<Category>) : RecyclerView.Adapter<CategoriesAdapter.CategoryViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return CategoryViewHolder(layoutInflater.inflate(
            R.layout.item_category, parent, false), parent.context)
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        holder.render(categories[position])

        val category : Category = categories[position]
        val binding = ItemCategoryBinding.bind(holder.itemView)

        binding.txtname.text = category.name
        if(category.status == "1")
        {
            binding.txtstatus.text ="Active"

        }
        else
        {
            binding.txtstatus.text ="Deactivate"

        }
    }

    override fun getItemCount(): Int {
        return categories.size
    }

    class CategoryViewHolder(view: View, ct: Context) : RecyclerView.ViewHolder(view) {

        private val binding = ItemCategoryBinding.bind(view)
        private val context = ct

        fun render(category: Category) {

            binding.categoryCard.setOnClickListener {
                var navController : NavController = Navigation.findNavController(binding.root)


                val action = CategoriesFragmentDirections.actionCategoriesFragmentToCategoryFragment(category.id)
                navController.navigate(action)
            }
        }
    }
}