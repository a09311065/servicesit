package mx.edu.uthermosillo.a09311065.ITServices.ui.views.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import mx.edu.uthermosillo.a09311065.ITServices.R
import mx.edu.uthermosillo.a09311065.ITServices.data.models.products.Product
import mx.edu.uthermosillo.a09311065.ITServices.databinding.ItemProductBinding
import mx.edu.uthermosillo.a09311065.ITServices.ui.views.products.ProductsFragmentDirections

class ProductsAdapter (private val products : List<Product>) : RecyclerView.Adapter<ProductsAdapter.ProductViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ProductViewHolder(layoutInflater.inflate(
            R.layout.item_product, parent, false), parent.context)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.render(products[position])

        val product : Product = products[position]
        val binding = ItemProductBinding.bind(holder.itemView)

        binding.txtcategory.text = product.category.name
        binding.txtname.text = product.name
        binding.txtdescription.text = product.description
        Picasso.get().load(product.image).into(binding.imageView)
    }

    override fun getItemCount(): Int {
        return products.size
    }

    class ProductViewHolder(view: View, ct: Context) : RecyclerView.ViewHolder(view) {

        private val binding = ItemProductBinding.bind(view)
        private val context = ct

        fun render(product: Product) {

            binding.productCard.setOnClickListener {
                //Toast.makeText(context, "Product " + product.id, Toast.LENGTH_SHORT).show()

                var navController : NavController = Navigation.findNavController(binding.root)

                //navController.navigate(R.id.action_productsFragment_to_productFragment, product.id)

                val action = ProductsFragmentDirections.actionProductsFragmentToProductFragment(product.id)
                navController.navigate(action)
            }
        }
    }
}