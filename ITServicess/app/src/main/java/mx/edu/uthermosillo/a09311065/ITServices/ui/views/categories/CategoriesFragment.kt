package mx.edu.uthermosillo.a09311065.ITServices.ui.views.categories

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import mx.edu.uthermosillo.a09311065.ITServices.databinding.FragmentCategoriesBinding
import mx.edu.uthermosillo.a09311065.ITServices.ui.viewmodels.CategoryViewModel
import mx.edu.uthermosillo.a09311065.ITServices.ui.views.adapters.CategoriesAdapter

class CategoriesFragment : Fragment() {


    private var _binding: FragmentCategoriesBinding? = null
    private val binding get() = _binding!!

    private val categoriesViewModel : CategoryViewModel by viewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentCategoriesBinding.inflate(inflater, container, false)

        categoriesViewModel.loadCategories()

        categoriesViewModel.categories.observe(viewLifecycleOwner, Observer { categories ->
            binding.categoriesRecycler.layoutManager = LinearLayoutManager(context)
            val adapter = CategoriesAdapter(categories)
            binding.categoriesRecycler.adapter = adapter

            if(categories.isNotEmpty()) {
                binding.categoriesShimmer.visibility = View.GONE
                binding.categoriesRecycler.visibility = View.VISIBLE
            }
        })

        categoriesViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            binding.categoriesShimmer.isVisible = it
        })




        return binding.root
    }
}