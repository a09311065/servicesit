package mx.edu.uthermosillo.a09311065.ITServices.ui.views.categories

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import mx.edu.uthermosillo.a09311065.ITServices.databinding.FragmentCategoryBinding
import mx.edu.uthermosillo.a09311065.ITServices.ui.viewmodels.CategoryViewModel

class CategoryFragment : Fragment() {

    private var _binding: FragmentCategoryBinding?=null
    private  val binding get() = _binding!!

    private val categoryViewModel : CategoryViewModel by viewModels()
    private var categoryId = 0
    private val args: CategoryFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentCategoryBinding.inflate(inflater, container, false)
        categoryId = args.categoryId

        categoryViewModel.loadCategory(categoryId)

        categoryViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            binding.categoriesShimmer.isVisible = it
            binding.content.isVisible = !it;
        })

        categoryViewModel.category.observe(viewLifecycleOwner, Observer {
                category ->
            binding.categoryName.text = category.name
            if(category.status == "1")
            {
                binding.categoryStatus.text ="Active"

            }
            else
            {
                binding.categoryStatus.text ="Deactivate"

            }
        })

        return binding.root
    }
}