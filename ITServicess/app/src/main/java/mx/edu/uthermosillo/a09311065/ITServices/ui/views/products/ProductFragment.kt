package mx.edu.uthermosillo.a09311065.ITServices.ui.views.products

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.squareup.picasso.Picasso
import mx.edu.uthermosillo.a09311065.ITServices.databinding.FragmentProductBinding
import mx.edu.uthermosillo.a09311065.ITServices.ui.viewmodels.ProductViewModel

class ProductFragment: Fragment() {

    private var _binding: FragmentProductBinding?=null
    private  val binding get() = _binding!!

    private val productViewModel : ProductViewModel by viewModels()
    private var productId = 0
    private val args: ProductFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentProductBinding.inflate(inflater, container, false)
        productId = args.productId

        productViewModel.loadProduct(productId)

        productViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            binding.productsShimmer.isVisible = it
            binding.content.isVisible = !it
        })

        productViewModel.product.observe(viewLifecycleOwner, Observer {
            product ->
            binding.productName.text = product.name
            binding.productDescription.text = product.description
            binding.productCategory.text = product.category.name
            Picasso.get().load(product.image).into(binding.productImage)
        })

        return binding.root
    }
}