package mx.edu.uthermosillo.a09311065.ITServices.ui.views.products

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import mx.edu.uthermosillo.a09311065.ITServices.databinding.FragmentProductsBinding
import mx.edu.uthermosillo.a09311065.ITServices.ui.viewmodels.ProductViewModel
import mx.edu.uthermosillo.a09311065.ITServices.ui.views.adapters.ProductsAdapter


class ProductsFragment : Fragment() {


    private var _binding: FragmentProductsBinding? = null
    private val binding get() = _binding!!

    private val productsViewModel : ProductViewModel by viewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentProductsBinding.inflate(inflater, container, false)

        productsViewModel.loadProducts()

        productsViewModel.products.observe(viewLifecycleOwner, Observer { products ->
            binding.productsRecycler.layoutManager = LinearLayoutManager(context)
            val adapter = ProductsAdapter(products)
            binding.productsRecycler.adapter = adapter

            if(products.isNotEmpty()) {
                binding.productsShimmer.visibility = View.GONE
                binding.productsRecycler.visibility = View.VISIBLE
            }
        })

        productsViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            binding.productsShimmer.isVisible = it
        })




        return binding.root
    }
}